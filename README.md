# Chat_SpringBoot

# Admin Interface Management

# Technology Stack:

1. Frontend: HTML, CSS, JavaScript
2. Frontend Dynamic Rendering: Thymeleaf
3. Implementation of Spring MVC Backend Architecture
4. API Definition for Frontend-Backend Communication: REST
5. Implementation of Spring Security

 
# Packages and Key Components

## 1. Controller Layer (`controller`)
- **AdminController**: Handles administrative operations, possibly related to user management and system settings.
- **ApiController**: Manages API-related actions, such as REST API requests for chat operations.
- **ChatController**: Core controller for handling chat functionalities including message sending and receiving.
- **HomeController**: Manages routes related to the home or dashboard views.

## 2. Data Access Layer (`dao`)
- Manages data interaction directly with the database, ensuring data access and manipulation are abstracted from the business logic.

## 3. Data Transfer Objects (`dto`)
- **ChannelDto**: Data transfer object for channel-related data.
- **UserDto**: Data transfer object for user-related data, used for transferring data between processes.

## 4. Exception Handling (`exception`)
- Contains custom exceptions that handle specific error cases throughout the application, improving error management and response strategies.

## 5. Model Layer (`model`)
- **Channel**: Represents the channel model with all its attributes.
- **ChatMessage**: Defines the structure of a chat message.
- **MessageType**: Enum defining types of messages (e.g., chat, join, leave).
- **User**: User model capturing all user-related information.

## 6. Security Configuration (`security`)
- **LoginFailureHandler**: Manages actions to take on login failure.
- **LoginSuccessHandler**: Defines actions upon successful login.
- **SecurityConfiguration**: Configures security settings, roles, and permissions.

## 7. Service Layer (`service`)
- **ChannelDtoService**, **EmailService**, **LoginAttemptService**, etc.: Services that implement the business logic of the application, handling everything from user login attempts to email notifications.

## 8. Utility Classes (`utility`)
- Utility classes that provide across-the-board helper functions used by various components of the application.

## 9. WebSocket Configuration (`websocket`)
- **WebSocketConfiguration**: Sets up WebSocket connections and configurations.
- **WebSocketEventListener**: Listens to WebSocket events such as connect and disconnect.

# Resources Structure

## `static` - Contains static resources used across the application.
1. `css` - Stylesheets for different components.

2. `images` - Images used in the application.

3. `javascript` - JavaScript files for interactivity.

## `templates` - HTML templates used for rendering views.
1. `admin` - Templates related to admin functionalities.
2. `chat` - Templates related to user functionalities.

## Tester le projet
-Après avoir lancé le projet Spring, vous pouvez accéder à la page principale de ChatApp en visitant http://localhost:8080/. 
-Ensuite, cliquez sur 'login' dans le coin supérieur droit pour accéder à l'interface de connexion. 
-Vous pourrez alors choisir de vous connecter en tant qu'utilisateur ou administrateur pour accéder à l'interface utilisateur ou à l'interface administrateur.



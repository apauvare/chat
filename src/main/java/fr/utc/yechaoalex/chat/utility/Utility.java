package fr.utc.yechaoalex.chat.utility;

import jakarta.servlet.http.HttpServletRequest;

/**
 * Contains utility methods for various purposes
 */
public class Utility {
    public static String getSiteURL(HttpServletRequest request) {
        String siteURL = request.getRequestURL().toString();
        return siteURL.replace(request.getServletPath(), "");
    }
}

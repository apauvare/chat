package fr.utc.yechaoalex.chat.dao;

import fr.utc.yechaoalex.chat.model.Channel;
import fr.utc.yechaoalex.chat.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    User findByEmail(String email);
    Optional<User> findById(Long id);

    Page<User> findByLastNameContainingIgnoreCaseOrFirstNameContainingIgnoreCase(String lastName, String firstName, Pageable pageable);

    @Query("UPDATE User u SET u.failedAttempts = ?1 WHERE u.email = ?2")
    @Modifying
    void updateFailedAttempts(int failAttempts, String email);

    User findByResetPasswordToken(String token);

    @Query("SELECT u FROM User u ORDER BY u.id DESC LIMIT 3")
    List<User> findTop3ByOrderByIdDesc();
}

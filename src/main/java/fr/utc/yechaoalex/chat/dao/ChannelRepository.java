package fr.utc.yechaoalex.chat.dao;

import fr.utc.yechaoalex.chat.model.Channel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ChannelRepository extends JpaRepository<Channel, Long> {
}

package fr.utc.yechaoalex.chat.model;

public enum MessageType {
    CHAT,
    JOIN,
    LEAVE
}

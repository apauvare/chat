package fr.utc.yechaoalex.chat.security;

import fr.utc.yechaoalex.chat.dao.UserRepository;
import fr.utc.yechaoalex.chat.model.User;
import fr.utc.yechaoalex.chat.service.LoginAttemptService;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * Responsible for handling authentication failures
 */
@Component
public class LoginFailureHandler extends SimpleUrlAuthenticationFailureHandler {
    @Autowired
    private LoginAttemptService loginAttemptService;

    @Autowired
    private UserRepository userRepository;

    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) throws IOException, ServletException {
        String email = request.getParameter("email");
        User user = userRepository.findByEmail(email);
        String role = request.getParameter("role");

        if (user != null) {
            if (user.isAccountNonLocked()) {
                if (user.getFailedAttempts() < LoginAttemptService.MAX_FAILED_ATTEMPTS - 1) {
                    loginAttemptService.increaseFailedAttempts(user);
                } else {
                    loginAttemptService.lock(user);
                    exception = new LockedException("Your account has been locked due to " + LoginAttemptService.MAX_FAILED_ATTEMPTS + " failed attempts." + " It will be unlocked after 24 hours.");
                }
            } else if (!user.isAccountNonLocked()) {
                if (loginAttemptService.unlockWhenTimeExpired(user)) {
                    exception = new LockedException("Your account has been unlocked. Please try to login again.");
                }
            }
        }
        if (role.equals("USER")){
            super.setDefaultFailureUrl("/login?errorUser");
        } else {
            super.setDefaultFailureUrl("/login?errorAdmin");
        }
        super.onAuthenticationFailure(request, response, exception);
    }
}

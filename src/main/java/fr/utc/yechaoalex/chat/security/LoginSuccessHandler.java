package fr.utc.yechaoalex.chat.security;

import fr.utc.yechaoalex.chat.model.User;
import fr.utc.yechaoalex.chat.service.SecurityUserDetails;
import fr.utc.yechaoalex.chat.service.LoginAttemptService;
import fr.utc.yechaoalex.chat.utility.JWTTokenUtil;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * Responsible for handling authentication success
 */
@Component
public class LoginSuccessHandler extends SavedRequestAwareAuthenticationSuccessHandler {
    @Autowired
    private LoginAttemptService loginAttemptService;

    @Autowired
    private JWTTokenUtil jwtTokenUtil;

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException {
        SecurityUserDetails userDetails = (SecurityUserDetails) authentication.getPrincipal();
        User user = userDetails.getUser();
        if (user.getFailedAttempts() > 0) {
            loginAttemptService.resetFailedAttempts(user.getEmail());
        }

        String redirectURL = request.getContextPath();
        String submittedRole = request.getParameter("role");

        if (submittedRole.equals("ADMIN") && userDetails.hasRole("ADMIN")) {
            redirectURL = "/admin/home";
        } else if (submittedRole.equals("ADMIN") && !userDetails.hasRole("ADMIN")) {
            redirectURL = "/login?wrongrole";
        } else if (submittedRole.equals("USER") && userDetails.hasRole("USER")) {
            // generate token
            String accessToken = jwtTokenUtil.generateAccessToken(user.getEmail());
            System.out.println("successful authentication");
            System.out.println("Generated token: " + accessToken);
            redirectURL = "/chat/home?token=" + accessToken + "&username=" + user.getEmail() +"&id=" + user.getId();
        }

        response.sendRedirect(redirectURL);
    }

}

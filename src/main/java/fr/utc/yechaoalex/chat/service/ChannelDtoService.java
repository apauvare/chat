package fr.utc.yechaoalex.chat.service;

import fr.utc.yechaoalex.chat.dao.ChannelRepository;
import fr.utc.yechaoalex.chat.dao.UserRepository;
import fr.utc.yechaoalex.chat.dto.ChannelDto;
import fr.utc.yechaoalex.chat.model.Channel;
import fr.utc.yechaoalex.chat.model.User;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class ChannelDtoService {
    @Autowired
    private ChannelRepository channelRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ModelMapper modelMapper;

    public List<ChannelDto> getAllChannelDtos() {
        List<Channel> foundChannels =  channelRepository.findAll();
        List<ChannelDto> channelDtos = new LinkedList<>();
        for (Channel channel : foundChannels) {
            ChannelDto newDto = modelMapper.map(channel, ChannelDto.class);
            newDto.setOwnerId(channel.getOwner().getId());
            channelDtos.add(newDto);
        }

        return channelDtos;
    }

    public ChannelDto getChannel(Long id) {
        Optional<Channel> foundChannel = channelRepository.findById(id);
        if (foundChannel.isPresent()) {
            ChannelDto channelDto = modelMapper.map(foundChannel.get(), ChannelDto.class);
            channelDto.setOwnerId(foundChannel.get().getOwner().getId());
            return channelDto;
        } else {
            return new ChannelDto();
        }
    }

    public List<ChannelDto> getChannelsByOwner(Long ownerId) {
        Optional<User> foundUser = userRepository.findById(ownerId);
        List<ChannelDto> channelDtos = new LinkedList<>();
        if (foundUser.isPresent()) {
            Set<Channel> channels = foundUser.get().getOwnerChannels();
            for (Channel channel : channels) {
                ChannelDto newDto = modelMapper.map(channel, ChannelDto.class);
                newDto.setOwnerId(ownerId);
                channelDtos.add(newDto);
            }
        }

        return channelDtos;
    }

    public List<ChannelDto> getChannelsByParticipant(Long userId) {
        Optional<User> foundUser = userRepository.findById(userId);
        List<ChannelDto> channelDtos = new LinkedList<>();
        if (foundUser.isPresent()) {
            Set<Channel> channels = foundUser.get().getInviteChannels();
            for (Channel channel : channels) {
                ChannelDto newDto = modelMapper.map(channel, ChannelDto.class);
                newDto.setOwnerId(channel.getOwner().getId());
                channelDtos.add(newDto);
            }
        }

        return channelDtos;
    }

    public ChannelDto createChannel(ChannelDto channelDto) {
        Channel channel = new Channel();
        channel.setTitle(channelDto.getTitle());
        channel.setDescription(channelDto.getDescription());
        channel.setDate(channelDto.getDate());
        channel.setDuration(channelDto.getDuration());
        Optional<User> user = userRepository.findById(channelDto.getOwnerId());
        if (user.isEmpty()) {
            return new ChannelDto();
        } else {
            channel.setOwner(user.get());
            channel = channelRepository.save(channel);
            ChannelDto response = modelMapper.map(channel, ChannelDto.class);
            response.setOwnerId(channel.getOwner().getId());

            return response;
        }
    }

    public void deleteChannel(Long channelId) {
        channelRepository.deleteById(channelId);
    }

    public ChannelDto updateChannel(Long id, ChannelDto channelDTO) {
        Optional<Channel> optionalChannel = channelRepository.findById(id);
        if (optionalChannel.isEmpty()) {
            return new ChannelDto();
        }

        Channel channel = optionalChannel.get();
        channel.setTitle(channelDTO.getTitle());
        channel.setDescription(channelDTO.getDescription());
        channel.setDate(channelDTO.getDate());
        channel.setDuration(channelDTO.getDuration());

        channel =  channelRepository.save(channel);
        ChannelDto response = modelMapper.map(channel, ChannelDto.class);
        response.setOwnerId(channel.getOwner().getId());

        return response;
    }

    public void addUserToChannel(Long userId, Long channelId) {
        Optional<User> foundUser = userRepository.findById(userId);
        if (foundUser.isEmpty()) {
            return;
        }
        Optional<Channel> foundChannel = channelRepository.findById(channelId);
        if (foundChannel.isEmpty()) {
            return;
        }

        User user = foundUser.get();
        Channel channel = foundChannel.get();

        user.getInviteChannels().add(channel);
        userRepository.save(user);

        channel.getParticipants().add(user);
        channelRepository.save(channel);
    }
}

package fr.utc.yechaoalex.chat.service;

import jakarta.mail.MessagingException;
import jakarta.mail.internet.MimeMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMailMessage;
import org.springframework.stereotype.Service;

/**
 * Sends different types of mail to user depending on need
 */
@Service
public class EmailService {
    private static final String EMAIL_SENDER = "abrg.0124@gmail.com";

    @Autowired
    private JavaMailSender mailSender;

    public void sendRegistrationEmail(String to, String temporaryPassword) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom(EMAIL_SENDER);
        message.setTo(to);
        message.setSubject("Your New Account");
        message.setText("Welcome to our chat application!\nIf you have received this message, it is because an administrator created an account for you.\nPlease change it after logging in using your temporary password: " + temporaryPassword);

        mailSender.send(message);
    }

    public void sendResetPasswordEmail(String to, String resetPasswordLink) throws MessagingException {
        MimeMessage message = mailSender.createMimeMessage();
        message.setFrom(EMAIL_SENDER);
        message.setRecipients(MimeMessage.RecipientType.TO, to);
        message.setSubject("Reset your password");

        String content = "<p>Hello,</p>"
                + "<p>You have requested to reset your password.</p>"
                + "<p>Click the link below to change your password:</p>"
                + "<p><a href=\"" + resetPasswordLink + "\">Change my password</a></p>"
                + "<br>"
                + "<p>Ignore this email if you do remember your password, "
                + "or if you have not made the request.</p>";
        message.setContent(content, "text/html; charset=utf-8");

        mailSender.send(message);
    }
}


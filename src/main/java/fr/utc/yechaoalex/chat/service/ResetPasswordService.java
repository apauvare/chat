package fr.utc.yechaoalex.chat.service;

import fr.utc.yechaoalex.chat.dao.UserRepository;
import fr.utc.yechaoalex.chat.model.User;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Class for handling case where user forgets password and wants to reset it
 */
@Service
@Transactional
public class ResetPasswordService {
    @Autowired
    UserRepository userRepository;

    public void updateResetPasswordToken(String token, String email) {
        User user = userRepository.findByEmail(email);
        if (user != null) {
            user.setResetPasswordToken(token);
            userRepository.save(user);
        }
    }

    public User getByResetPasswordToken(String token) {
        return userRepository.findByResetPasswordToken(token);
    }

    public void updatePassword(User user, String newPassword) {
        user.setPassword(newPassword);

        user.setResetPasswordToken(null);
        userRepository.save(user);
    }
}

package fr.utc.yechaoalex.chat.service;

import fr.utc.yechaoalex.chat.dao.UserRepository;
import fr.utc.yechaoalex.chat.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * Handles the pagination system used in user listing
 */
@Service
public class UserPaginationService {
    @Autowired
    private UserRepository userRepository;

    public Page<User> getUsers(Optional<Integer> page, Optional<Integer> size, Optional<String> sort, Optional<String> order, Optional<String> name) {
        Sort sortCriteria;
        Sort.Direction chosenOrder;
        String chosenSort;
        chosenSort = sort.orElse("id");
        if (order.isEmpty()) {
            chosenOrder = Sort.Direction.ASC;
        } else {
            if (order.get().equalsIgnoreCase("ascending")) {
                chosenOrder = Sort.Direction.ASC;
            } else if (order.get().equalsIgnoreCase("descending")) {
                chosenOrder = Sort.Direction.DESC;
            } else {
                chosenOrder = Sort.Direction.ASC;
            }
        }

        sortCriteria = Sort.by(chosenOrder, chosenSort);
        Pageable pageable = PageRequest.of(page.orElse(0), size.orElse(5), sortCriteria);

        return userRepository.findByLastNameContainingIgnoreCaseOrFirstNameContainingIgnoreCase(name.orElse(""), name.orElse(""), pageable);
    }
}

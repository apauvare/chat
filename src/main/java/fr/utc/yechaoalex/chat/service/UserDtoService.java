package fr.utc.yechaoalex.chat.service;

import fr.utc.yechaoalex.chat.dao.ChannelRepository;
import fr.utc.yechaoalex.chat.dao.UserRepository;
import fr.utc.yechaoalex.chat.dto.ChannelDto;
import fr.utc.yechaoalex.chat.dto.UserDto;
import fr.utc.yechaoalex.chat.model.Channel;
import fr.utc.yechaoalex.chat.model.User;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class UserDtoService {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ChannelRepository channelRepository;

    @Autowired
    private ModelMapper modelMapper;

    public UserDto getUserDto(Long id) {
        Optional<User> foundUser = userRepository.findById(id);
        if (foundUser.isPresent()) {
            return modelMapper.map(foundUser.get(), UserDto.class);
        } else {
            return new UserDto();
        }
    }

    public List<UserDto> getAllUserDtos() {
        List<User> users = userRepository.findAll();
        List<UserDto> usersDto = new LinkedList<>();
        for (User user : users) {
            UserDto currentDto = modelMapper.map(user, UserDto.class);
            usersDto.add(currentDto);
        }

        return usersDto;
    }

    public List<UserDto> getUserDtosByInviteChannel(Long channelId) {
        Optional<Channel> foundChannel = channelRepository.findById(channelId);
        List<UserDto> userDtos = new LinkedList<>();
        if (foundChannel.isPresent()) {
            Set<User> users = foundChannel.get().getParticipants();
            for (User user : users) {
                UserDto userDto = modelMapper.map(user, UserDto.class);
                userDtos.add(userDto);
            }
        }

        return userDtos;
    }
}

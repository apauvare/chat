package fr.utc.yechaoalex.chat.controller;

import fr.utc.yechaoalex.chat.model.ChatMessage;
import fr.utc.yechaoalex.chat.model.MessageType;
import fr.utc.yechaoalex.chat.utility.JWTTokenUtil;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ChatController {

    @Autowired
    private SimpMessageSendingOperations messagingTemplate;

    @GetMapping("/chat/home")
    public ModelAndView displayHomePage(@RequestParam String token, @RequestParam String username, @RequestParam Long id) {
        System.out.println(token);

        ModelAndView modelAndView = new ModelAndView();
//        modelAndView.setViewName("chat/chat_home");
        modelAndView.setViewName("redirect:http://localhost:3000?token=" + token + "&username=" + username + "&id=" + id);

        return modelAndView;
    }

    @MessageMapping("/chat/{roomId}/sendMessage")
    public void sendMessage(@DestinationVariable String roomId, @Payload ChatMessage chatMessage) {
        System.out.println("Message received: " + chatMessage.getContent());
        messagingTemplate.convertAndSend("/channel/" + roomId, chatMessage);
    }

    @MessageMapping("/chat/{roomId}/addUser")
    public void addUser(@DestinationVariable String roomId, @Payload ChatMessage chatMessage, SimpMessageHeaderAccessor headerAccessor) {
        String currentRoomId = (String) headerAccessor.getSessionAttributes().put("roomId", roomId);
        if (currentRoomId != null) {
            ChatMessage leaveMessage = new ChatMessage();
            leaveMessage.setType(MessageType.LEAVE);
            leaveMessage.setSender(chatMessage.getSender());
            messagingTemplate.convertAndSend("/channel/" + currentRoomId, leaveMessage);
        }

        headerAccessor.getSessionAttributes().put("username", chatMessage.getSender());
        messagingTemplate.convertAndSend("/channel/" + roomId, chatMessage);
    }
}

package fr.utc.yechaoalex.chat.controller;

import fr.utc.yechaoalex.chat.dao.UserRepository;
import fr.utc.yechaoalex.chat.model.User;
import fr.utc.yechaoalex.chat.service.EmailService;
import fr.utc.yechaoalex.chat.service.ResetPasswordService;
import fr.utc.yechaoalex.chat.utility.RandomString;
import fr.utc.yechaoalex.chat.utility.Utility;
import jakarta.mail.MessagingException;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(value="/")
public class HomeController {
    private static final int MIN_PASSWORD_LENGTH = 7;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private EmailService emailService;

    @Autowired
    private ResetPasswordService resetPasswordService;

    @GetMapping(value="")
    public ModelAndView displayHomePage() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("index");

        return modelAndView;
    }

    @GetMapping(value="login")
    public ModelAndView displayLoginPage() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("login");

        return modelAndView;
    }

    @GetMapping(value = "forgotPassword")
    public ModelAndView displayForgotPasswordPage() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("forgot_password");

        return modelAndView;
    }

    @PostMapping(value = "forgotPassword")
    public ModelAndView processForgotPassword(HttpServletRequest request) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("forgot_password");
        String email = request.getParameter("email");
        RandomString randomStringGenerator = new RandomString();
        String token = randomStringGenerator.nextString();

        User user = userRepository.findByEmail(email);
        if (user != null) {
            try {
                resetPasswordService.updateResetPasswordToken(token, email);
                String resetPasswordLink = Utility.getSiteURL(request) + "/resetPassword?token=" + token;
                emailService.sendResetPasswordEmail(email, resetPasswordLink);
                modelAndView.addObject("message", "We have sent a reset password link to your email address. Please check your email.");
            } catch (MessagingException exception) {
                System.err.println(exception.getMessage());
                modelAndView.addObject("error", exception.getMessage());
            }
        } else {
            modelAndView.addObject("message", "You are not an user.");
        }

        return modelAndView;
    }

    @GetMapping(value = "resetPassword")
    public ModelAndView displayResetPasswordPage(@Param(value = "token") String token) {
        ModelAndView modelAndView = new ModelAndView();

        User user = resetPasswordService.getByResetPasswordToken(token);
        modelAndView.addObject("token", token);

        if (user == null) {
            modelAndView.addObject("message", "Invalid Token");
            modelAndView.setViewName("message");
        } else {
            modelAndView.setViewName("reset_password");
        }

        return modelAndView;
    }

    @PostMapping(value = "resetPassword")
    public ModelAndView processResetPassword(HttpServletRequest request) {
        ModelAndView modelAndView = new ModelAndView();
        String token = request.getParameter("token");
        String password = request.getParameter("password");

        // Password not long enough
        if (password.length() < MIN_PASSWORD_LENGTH) {
            modelAndView.setViewName("reset_password");
            modelAndView.addObject("token", token);
            modelAndView.addObject("errorMessage", "Password must contain at least " + MIN_PASSWORD_LENGTH + " characters.");
            return modelAndView;
        }

        User user = resetPasswordService.getByResetPasswordToken(token);
        modelAndView.setViewName("message");
        modelAndView.addObject("title", "Reset Your Password");

        if (user == null) {
            modelAndView.addObject("message", "Invalid Token");
        } else {
            resetPasswordService.updatePassword(user, password);
            modelAndView.addObject("message", "You have successfully changed your password.");
        }

        return modelAndView;
    }
}

package fr.utc.yechaoalex.chat.controller;

import fr.utc.yechaoalex.chat.dto.ChannelDto;
import fr.utc.yechaoalex.chat.dto.UserDto;
import fr.utc.yechaoalex.chat.model.Channel;
import fr.utc.yechaoalex.chat.service.ChannelDtoService;
import fr.utc.yechaoalex.chat.service.UserDtoService;
import fr.utc.yechaoalex.chat.utility.JWTTokenUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value="/api")
public class ApiController {
    @Autowired
    private UserDtoService userDtoService;

    @Autowired
    private ChannelDtoService channelDtoService;

    @Autowired
    private JWTTokenUtil jwtTokenUtil;

    @GetMapping("users/all")
    public ResponseEntity<List<UserDto>> getAllUserDtos(@RequestHeader(name="Authorization") String token, @RequestParam String username) {
        // The received token starts with "Bearer", so we remove it
        String rawToken = token.substring(7);
        System.out.println("Received token: " + rawToken);
        boolean tokenVerification = jwtTokenUtil.validateToken(rawToken, username);
        if (!tokenVerification) {
            return new ResponseEntity<>(new ArrayList<>(), HttpStatus.FORBIDDEN);
        }

        return new ResponseEntity<>(userDtoService.getAllUserDtos(), HttpStatus.OK);
    }

    @GetMapping("users/{id}")
    public ResponseEntity<UserDto> getUserDto(@PathVariable Long id, @RequestHeader(name="Authorization") String token, @RequestParam String username) {
        // The received token starts with "Bearer", so we remove it
        String rawToken = token.substring(7);
        boolean tokenVerification = jwtTokenUtil.validateToken(rawToken, username);
        if (!tokenVerification) {
            return new ResponseEntity<>(new UserDto(), HttpStatus.FORBIDDEN);
        }

        UserDto foundUser =  userDtoService.getUserDto(id);
        if (foundUser == null || foundUser.getEmail() == null) {
            return new ResponseEntity<>(foundUser, HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<>(foundUser, HttpStatus.OK);
        }
    }

    /**
     * Get all channels owned by a user
     * @param id
     * @param token
     * @param username
     * @return
     */
    @GetMapping("users/{id}/owner_channels")
    public ResponseEntity<List<ChannelDto>> getOwnerChannels(@PathVariable Long id, @RequestHeader(name="Authorization") String token, @RequestParam String username) {
        // The received token starts with "Bearer", so we remove it
        String rawToken = token.substring(7);
        boolean tokenVerification = jwtTokenUtil.validateToken(rawToken, username);
        if (!tokenVerification) {
            return new ResponseEntity<>(new ArrayList<>(), HttpStatus.FORBIDDEN);
        }

        UserDto foundUser =  userDtoService.getUserDto(id);
        if (foundUser == null || foundUser.getEmail() == null) {
            return new ResponseEntity<>(new LinkedList<>(), HttpStatus.NOT_FOUND);
        } else {
            List<ChannelDto> channelDtos = channelDtoService.getChannelsByOwner(id);
            return new ResponseEntity<>(channelDtos, HttpStatus.OK);
        }
    }

    /**
     * Get all channels where user is invited
     * @param id
     * @param token
     * @param username
     * @return
     */
    @GetMapping("users/{id}/invite_channels")
    public ResponseEntity<List<ChannelDto>> getInviteChannels(@PathVariable Long id, @RequestHeader(name="Authorization") String token, @RequestParam String username) {
        // The received token starts with "Bearer", so we remove it
        String rawToken = token.substring(7);
        boolean tokenVerification = jwtTokenUtil.validateToken(rawToken, username);
        if (!tokenVerification) {
            return new ResponseEntity<>(new ArrayList<>(), HttpStatus.FORBIDDEN);
        }

        UserDto foundUser = userDtoService.getUserDto(id);
        if (foundUser == null || foundUser.getEmail() == null) {
            return new ResponseEntity<>(new LinkedList<>(), HttpStatus.NOT_FOUND);
        } else {
            List<ChannelDto> channelDtos = channelDtoService.getChannelsByParticipant(id);
            return new ResponseEntity<>(channelDtos, HttpStatus.OK);
        }
    }

    @GetMapping("channels/all")
    public ResponseEntity<List<ChannelDto>> getAllChannels(@RequestHeader(name="Authorization") String token, @RequestParam String username) {
        // The received token starts with "Bearer", so we remove it
        String rawToken = token.substring(7);
        boolean tokenVerification = jwtTokenUtil.validateToken(rawToken, username);
        if (!tokenVerification) {
            return new ResponseEntity<>(new ArrayList<>(), HttpStatus.FORBIDDEN);
        }

        return new ResponseEntity<>(channelDtoService.getAllChannelDtos(), HttpStatus.OK);
    }

    @GetMapping("channels/{id}")
    public ResponseEntity<ChannelDto> getChannel(@PathVariable Long id, @RequestHeader(name="Authorization") String token, @RequestParam String username) {
        // The received token starts with "Bearer", so we remove it
        String rawToken = token.substring(7);
        boolean tokenVerification = jwtTokenUtil.validateToken(rawToken, username);
        if (!tokenVerification) {
            return new ResponseEntity<>(new ChannelDto(), HttpStatus.FORBIDDEN);
        }

        ChannelDto channelDto = channelDtoService.getChannel(id);
        if (channelDto.getTitle() == null) {
            return new ResponseEntity<>(new ChannelDto(), HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<>(channelDto, HttpStatus.OK);
        }
    }

    @PostMapping("channels")
    public ResponseEntity<ChannelDto> createChannel(@RequestBody ChannelDto channelDto, @RequestHeader(name="Authorization") String token, @RequestParam String username) {
        // The received token starts with "Bearer", so we remove it
        String rawToken = token.substring(7);
        boolean tokenVerification = jwtTokenUtil.validateToken(rawToken, username);
        if (!tokenVerification) {
            return new ResponseEntity<>(new ChannelDto(), HttpStatus.FORBIDDEN);
        }

        ChannelDto response = channelDtoService.createChannel(channelDto);
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @DeleteMapping("channels/{id}")
    public ResponseEntity<ChannelDto> deleteChannel(@PathVariable Long id, @RequestHeader(name="Authorization") String token, @RequestParam String username) {
        // The received token starts with "Bearer", so we remove it
        String rawToken = token.substring(7);
        boolean tokenVerification = jwtTokenUtil.validateToken(rawToken, username);
        if (!tokenVerification) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }

        channelDtoService.deleteChannel(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    /**
     * Update a channel
     * @param id
     * @param channelDto
     * @param token
     * @param username
     * @return
     */
    @PutMapping("channels/{id}")
    public ResponseEntity<ChannelDto> modifyChannel(@PathVariable Long id, @RequestBody ChannelDto channelDto, @RequestHeader(name="Authorization") String token, @RequestParam String username) {
        // The received token starts with "Bearer", so we remove it
        String rawToken = token.substring(7);
        boolean tokenVerification = jwtTokenUtil.validateToken(rawToken, username);
        if (!tokenVerification) {
            return new ResponseEntity<>(new ChannelDto(), HttpStatus.FORBIDDEN);
        }

        ChannelDto updatedChannel = channelDtoService.updateChannel(id, channelDto);

        return new ResponseEntity<>(updatedChannel, HttpStatus.OK);
    }

    @GetMapping("channels/{id}/invited_users")
    public ResponseEntity<List<UserDto>> getInvitedUsersFromChannel(@PathVariable Long id, @RequestHeader(name="Authorization") String token, @RequestParam String username) {
        // The received token starts with "Bearer", so we remove it
        String rawToken = token.substring(7);
        boolean tokenVerification = jwtTokenUtil.validateToken(rawToken, username);
        if (!tokenVerification) {
            return new ResponseEntity<>(new ArrayList<>(), HttpStatus.FORBIDDEN);
        }

        // add channel owner
        ChannelDto channelDto = channelDtoService.getChannel(id);
        if (channelDto.getTitle() == null) {
            return new ResponseEntity<>(new ArrayList<>(), HttpStatus.NOT_FOUND);
        } else {
            Long ownerId = channelDto.getOwnerId();
            UserDto ownerDto = userDtoService.getUserDto(ownerId);
            List<UserDto> users = userDtoService.getUserDtosByInviteChannel(id);
            users.add(ownerDto);
            return new ResponseEntity<>(users, HttpStatus.OK);
        }
    }

    @PutMapping("channels/{channelId}/add_user/{userId}")
    public ResponseEntity<ChannelDto> addUserToChannel(@PathVariable Long channelId, @PathVariable Long userId, @RequestHeader(name="Authorization") String token, @RequestParam String username) {
        // The received token starts with "Bearer", so we remove it
        String rawToken = token.substring(7);
        boolean tokenVerification = jwtTokenUtil.validateToken(rawToken, username);
        if (!tokenVerification) {
            return new ResponseEntity<>(new ChannelDto(), HttpStatus.FORBIDDEN);
        }

        // Check if user exists
        UserDto foundUser =  userDtoService.getUserDto(userId);
        if (foundUser == null || foundUser.getEmail() == null) {
            return new ResponseEntity<>(new ChannelDto(), HttpStatus.NOT_FOUND);
        }

        // Check if channel exists
        ChannelDto foundChannel = channelDtoService.getChannel(channelId);
        if (foundChannel == null || foundChannel.getId() == null) {
            return new ResponseEntity<>(new ChannelDto(), HttpStatus.NOT_FOUND);
        }

        channelDtoService.addUserToChannel(userId, channelId);

        return new ResponseEntity<>(new ChannelDto(), HttpStatus.OK);
    }
}

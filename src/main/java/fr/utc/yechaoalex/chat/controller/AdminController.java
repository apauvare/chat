package fr.utc.yechaoalex.chat.controller;

import fr.utc.yechaoalex.chat.dao.UserRepository;
import fr.utc.yechaoalex.chat.model.User;
import fr.utc.yechaoalex.chat.service.EmailService;
import fr.utc.yechaoalex.chat.service.UserPaginationService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping(value = "/admin")
public class AdminController {
    private static final int MIN_PASSWORD_LENGTH = 7;

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UserPaginationService userPaginationService;
    @Autowired
    private EmailService emailService;

    @GetMapping(value={"home", "/"})
    public ModelAndView displayAdminPage() {
        ModelAndView model = new ModelAndView();
        model.setViewName("admin/admin_home");

        return model;
    }

    @GetMapping("dashboard")
    public ModelAndView displayDashboard() {
        ModelAndView model = new ModelAndView();
        model.setViewName("admin/admin_dashboard");

        List<User> latestUsers = userRepository.findTop3ByOrderByIdDesc();
        model.addObject("latestUsers", latestUsers);

        return model;
    }

    @GetMapping("listUsers")
    public ModelAndView displayUsers(@RequestParam(name="page") Optional<Integer> page, @RequestParam(name="size") Optional<Integer> size,
                                     @RequestParam(name="sort") Optional<String> sort, @RequestParam(name="order") Optional<String> order,
                                     @RequestParam(name="name") Optional<String> name) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("admin/admin_list_users");

        String url = "/admin/listUsers";
        modelAndView.addObject("url", url);

        Page<User> usersPage = userPaginationService.getUsers(page, size, sort, order, name);
        modelAndView.addObject("usersPage", usersPage);
        modelAndView.addObject("sort", sort.orElse("id"));
        modelAndView.addObject("order", order.orElse("ascending"));
        modelAndView.addObject("page", page.orElse(0));
        modelAndView.addObject("size", size.orElse(5));
        modelAndView.addObject("name", name.orElse(""));

        return modelAndView;
    }

    @GetMapping("createUser")
    public ModelAndView createUser() {
        ModelAndView model = new ModelAndView();
        model.setViewName("admin/admin_create_user");

        return model;
    }

    @PostMapping(value="createUser")
    public ModelAndView createUser(@RequestParam(value="firstName") String firstName, @RequestParam(value="lastName") String lastName, @RequestParam(value="email") String email, @RequestParam(value="password") String password) {
        ModelAndView modelAndView = new ModelAndView();
        User emailResult = userRepository.findByEmail(email);

        if (emailResult != null) {
            // User already exists
            modelAndView.setViewName("admin/admin_create_user");
            modelAndView.addObject("errorMessage", "User already exists");
        } else {
            if (password.length() < MIN_PASSWORD_LENGTH) {
                // Password not long enough
                modelAndView.setViewName("admin/admin_create_user");
                modelAndView.addObject("errorMessage", "Password must contain at least " + MIN_PASSWORD_LENGTH + " characters.");
            } else {
                // Create new user
                User newUser = new User(firstName, lastName, email, password, false, true);
                userRepository.save(newUser);
                emailService.sendRegistrationEmail(newUser.getEmail(), newUser.getPassword());
                modelAndView.setViewName("admin/admin_create_user_success");
                modelAndView.addObject("firstName", firstName);
                modelAndView.addObject("lastName", lastName);
                modelAndView.addObject("email", email);
            }
        }

        return modelAndView;
    }

    @GetMapping("/deactivate")
    public String deactivateUser(@RequestParam Long userId) {
        User user = userRepository.findById(userId).orElse(null);
        if (user != null) {
            user.setAccountNonLocked(false);
            user.setLockTime(new Date());
            userRepository.save(user);
            //            return ResponseEntity.ok("Activation successful");
        } else {
            //            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Activation failed");
        }

        return "redirect:/admin/listUsers";
    }

    @GetMapping("/activate")
    public String activateUser(@RequestParam Long userId) {
        User user = userRepository.findById(userId).orElse(null);
        if (user != null) {
            user.setAccountNonLocked(true);
            user.setLockTime(null);
            user.setFailedAttempts(0);
            userRepository.save(user);
            //            return ResponseEntity.ok("Activation successful");
        } else {
            //            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Activation failed");
        }

        return "redirect:/admin/listUsers";
    }

    @GetMapping("/userEdit")
    public String userEdit(@RequestParam Long userId,@RequestParam(value="firstName") String firstName, @RequestParam(value="lastName")String lastName) {
        User user = userRepository.findById(userId).orElse(null);
        if (user != null) {
            user.setFirstName(firstName);
            user.setLastName(lastName);
            userRepository.save(user);
            //            return ResponseEntity.ok("Activation successful");
        } else {
            //            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Activation failed");
        }

        return "redirect:/admin/listUsers";
    }

    @GetMapping("/userDelete")
    public String userDelete(@RequestParam Long userId) {
        try {
            userRepository.deleteById(userId);
            return "redirect:/admin/listUsers";
        } catch (Exception e) {
            // TODO : user deletion exception
            return "redirect:/admin/listUsers";
        }
    }

    @GetMapping(value="loggedInUser")
    public ModelAndView displayCurrentUserInformation() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("admin/admin_current_user");

        return modelAndView;
    }
}

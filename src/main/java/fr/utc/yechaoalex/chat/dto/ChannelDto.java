package fr.utc.yechaoalex.chat.dto;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class ChannelDto {
    private Long id;
    private String title;
    private String description;
    private Date date;
    private Integer duration;
    private Long ownerId;

    public ChannelDto() {
        super();
    }

    public ChannelDto(Long id, String title, String description, Date date, Integer duration, Long owner) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.date = date;
        this.duration = duration;
        this.ownerId = owner;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public Long getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(Long ownerId) {
        this.ownerId = ownerId;
    }
}

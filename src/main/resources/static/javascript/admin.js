const sideMenu = document.querySelector('aside');
const menuButton = document.getElementById('menu-btn');
const closeButton = document.getElementById('close-btn');

const darkMode = document.querySelector('.dark-mode');

var sideLinksContainer = document.querySelector('.sidebar');
sideLinksContainer.getElementsByClassName("side-link");
menuButton.addEventListener('click', () => {
    sideMenu.style.display = 'block';
});

closeButton.addEventListener('click', () => {
    sideMenu.style.display = 'none';
})

darkMode.addEventListener('click', () => {
    document.body.classList.toggle('dark-mode-variables');
    darkMode.querySelector('span:nth-child(1)').classList.toggle('active');
    darkMode.querySelector('span:nth-child(2)').classList.toggle('active');
})

document.addEventListener("DOMContentLoaded", function() {
    const links = document.querySelectorAll(".side-link");

    links.forEach(link => {
        if (window.location.pathname === link.getAttribute("href")) {
            link.classList.add('active');
        } else {
            link.classList.remove('active');
        }
    });
});